Source: rw
Maintainer: Debian Science Maintainers <debian-science-maintainers@alioth-lists.debian.net>
Uploaders: Julien Puydt <jpuydt@debian.org>
Section: math
Priority: optional
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://sourceforge.net/projects/rankwidth
Build-Depends: debhelper-compat (= 13), pkg-config
Vcs-Git: https://salsa.debian.org/science-team/rw.git
Vcs-Browser: https://salsa.debian.org/science-team/rw

Package: librw0
Architecture: any
Section: libs
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Compute rank-width and rank-decompositions of graphs
 Compute rank-width and rank-decompositions of graphs. It is based on
 ideas from "Computing rank-width exactly" by Sang-il Oum, "Sopra una
 formula numerica" by Ernesto Pascal, "Generation of a Vector from the
 Lexicographical Index" by B.P. Buckles and M. Lybanon and "Fast
 additions on masked integers" by Michael D. Adams and David S. Wise.
 .
 This package contains the library itself.

Package: librw-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: librw0 (= ${binary:Version}), ${misc:Depends}
Description: Compute rank-width and rank-decompositions of graphs (development)
 Compute rank-width and rank-decompositions of graphs. It is based on
 ideas from "Computing rank-width exactly" by Sang-il Oum, "Sopra una
 formula numerica" by Ernesto Pascal, "Generation of a Vector from the
 Lexicographical Index" by B.P. Buckles and M. Lybanon and "Fast
 additions on masked integers" by Michael D. Adams and David S. Wise.
 .
 This package contains the development files for the library.
